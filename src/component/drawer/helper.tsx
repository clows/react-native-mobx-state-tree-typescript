'use strict'
var React = require('react-native');
import Reactotron from "reactotron-react-native";
var {
   Animated,
   Easing
} = React;


export default class DrawerHelper {
   initialUsedSpace: any;
   tension:any ;
   friction:any;
   initialPosition: any;
   callbackPositionUpdated: any;
   screen_height:number;

   constructor(screen_height) {
    this.screen_height = screen_height;
}
    calculateInitialPosition( initial_used_space ){
      this.initialUsedSpace = Math.abs(initial_used_space);
      this.initialPosition = (this.screen_height * ( 1 - this.initialUsedSpace ));
      return this.initialPosition;
   };

    getInitialUsedSpace(){
      return this.initialUsedSpace;
   };

    getInitialPosition(){
      return this.initialPosition;
   };


    setupAnimation ( higher_tension, friction, callbackPositionUpdated ){
      this.tension = higher_tension;
      this.friction = friction;
      this.callbackPositionUpdated = callbackPositionUpdated;

   };


    isAValidMovement(distanceX, distanceY){
      var moveTravelledFarEnough =  Math.abs(distanceY) > Math.abs(distanceX) && Math.abs(distanceY) > 2;
      return moveTravelledFarEnough;
   };


    startAnimation (velocityY, positionY,initialPositon,id ){
        Reactotron.log('creating animation ');
      var isGoingToUp = ( velocityY < 0 )? true : false;
      var speed = Math.abs(velocityY);
      var currentPosition = Math.abs(positionY / this.screen_height);
      var endPosition = isGoingToUp? 0 : initialPositon;

      var position = new Animated.Value(positionY);
      position.removeAllListeners();

      Reactotron.log('configuration : '+endPosition)

      Animated.timing(position, {
         toValue: endPosition,
         tension: 30,
         friction: 1,
         //easing:Easing.elastic,
         velocity: velocityY
      }).start();

      position.addListener((position)=>{Reactotron.log('position by {position} {endPosition}');});
      position.addListener(this.callbackPositionUpdated);
   };
};
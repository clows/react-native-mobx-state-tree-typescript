var TENSION = 800;
var FRICTION = 90;

import React, { Component } from 'react'
import Reactotron from "reactotron-react-native";

import {
   AppRegistry,
   StyleSheet,
   TouchableWithoutFeedback,
   Text,
   Image,
   View,
   Animated,
   AlertIOS,
   PanResponder,
   Dimensions,
   
} from 'react-native';

import DraggableDrawerHelperImport from './helper';

// import DraggableDrawerHelper from "./helper"(123);
var SCREEN_HEIGHT = Dimensions.get('window').height;

// var DraggableDrawerHelper = require('./helper')(SCREEN_HEIGHT);
var DraggableDrawerHelper = new DraggableDrawerHelperImport(SCREEN_HEIGHT);


export interface Props {
	initialDrawerSize: number;
    onInitialPositionReached: null | any;
    onRelease:any;
    renderContainerView: () => null | JSX.Element;
    renderDrawerView: () => null | JSX.Element;
    onDragDown: any;
}
export interface State {
    position: any;
    initialPositon: any;
    touched: any;
}


export default class Drawer extends Component<Props, State> {
    _previousTop:any;
    center:any;
    _panGesture: any;

    constructor(props: Props) {
        super(props);
        Reactotron.log('Drawer.getInitialState');
      // naming it initialX clearly indicates that the only purpose
      // of the passed down prop is to initialize something internally
      var initialDrawerSize = DraggableDrawerHelper.calculateInitialPosition(this.props.initialDrawerSize);
      console.log(initialDrawerSize,'Initial size');
      this.state = {
         touched:'FALSE',
         position: new Animated.Value(initialDrawerSize),
         initialPositon:initialDrawerSize
      };
   }

   onUpdatePosition (position){
      this.state.position.setValue(position);
      this._previousTop = position;
      console.log('Position ',position);
      var initialPosition = DraggableDrawerHelper.getInitialPosition();

      if(initialPosition === position){
         this.props.onInitialPositionReached && this.props.onInitialPositionReached();
      }
   }

   componentWillMount() {
    Reactotron.log('Drawer.componentWillMount');
      // Initialize the DraggableDrawerHelper that will drive animations
      DraggableDrawerHelper.setupAnimation(TENSION,FRICTION,
         (position) => {
            if (!this.center) return;
            this.onUpdatePosition(position.value);
         }
      );

      this._panGesture = PanResponder.create({
         onMoveShouldSetPanResponder: (_, gestureState) => {
            return  DraggableDrawerHelper.isAValidMovement(gestureState.dx,gestureState.dy) && this.state.touched=='TRUE';
         },
         onPanResponderMove: (_, gestureState) => {
            this.moveDrawerView(gestureState)
         },
         onPanResponderRelease: (_, gestureState) => {
            this.moveFinished(gestureState)
         },
      })
   }


   moveDrawerView(gestureState) {
      console.log(gestureState.vy,'GESTURE');
      if (!this.center) return;
      var currentValue = Math.abs(gestureState.moveY / SCREEN_HEIGHT);
      var isGoingToUp = ( gestureState.vy < 0 );
      //Here, I'm subtracting %5 of screen size from edge drawer position to be closer as possible to finger location when dragging the drawer view
      var position = gestureState.moveY - SCREEN_HEIGHT * 0.05;
      //Send to callback function the current drawer position when drag down the drawer view component
      if(!isGoingToUp) this.props.onDragDown(1-currentValue);
      this.onUpdatePosition(position);
   }

   moveFinished(gestureState) {
      var isGoingToUp = ( gestureState.vy < 0 );
      if (!this.center) return;
      DraggableDrawerHelper.startAnimation(gestureState.vy,gestureState.moveY,this.state.initialPositon,gestureState.stateId);
      this.props.onRelease && this.props.onRelease(isGoingToUp);
   }

   render() {
    Reactotron.log('Drawer.render');
      var containerView = this.props.renderContainerView ? this.props.renderContainerView() : null;
      var drawerView = this.props.renderDrawerView ? this.props.renderDrawerView() : null;

      var drawerPosition = {
         top: this.state.position
      };
      
      return (
         <View style={styles.viewport}>
            <Animated.View
               style={[drawerPosition, styles.drawer]}
               ref={(center) => this.center = center}
               {...this._panGesture.panHandlers}>
               <TouchableWithoutFeedback
                  onPressIn={()=>{this.setState({touched:'TRUE'})}}
                  onPressOut={()=>{this.setState({touched:'FALSE'})}}>
                  <View style={{height:40,paddingTop:40,marginTop:20,backgroundColor:'rgba(0, 0, 0, 0.51)',borderTopLeftRadius:15,borderTopRightRadius:15}}>
                  
                  </View>
               </TouchableWithoutFeedback>
               {drawerView}
            </Animated.View>
         </View>
      )
   }
}


var styles = StyleSheet.create({
   viewport: {
      flex: 1,
   },
   drawer: {
      flex:1,
   },
   container: {
      position: 'absolute',
      top:0,
      left:0,
      bottom: 0,
      right: 0,
   },
});

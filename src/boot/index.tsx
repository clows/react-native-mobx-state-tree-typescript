import config from "./configureStore";
import app from "./setup";

import { mst } from "reactotron-mst";
import Reactotron from "reactotron-react-native";

export default function() {
  Reactotron.configure() // controls connection & communication settings
    .useReactNative() // add all built-in react native plugins
    .use(mst())
    .connect(); // let's connect!

  Reactotron.log("hello rendering world");

  const stores = config();
  Reactotron.trackMstNode(stores);

  return app(stores);
}
